import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';


import { postActions } from '../_actions';

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            content: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        // e.preventDefault();
        this.setState({ submitted: true });
        const { content } = this.state;
        if (content) {
            this.props.submitPost(content);
        }
    }

    render() {
        const { user } = this.props;
        const { content, submitted } = this.state
        return (
            <div>
                <div className="well">
                    <h1>Hi {user.username}!</h1>
                    <span><Link to="/login">Logout</Link></span>   
                </div>
                <div className="well">
                    <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group' + (submitted && !content ? ' has-error' : '')}>
                            <label htmlFor="content">Share Your Thoughts</label>
                            <textarea className="form-control" name="content" rows="5" col="50" onChange={this.handleChange} ></textarea>
                            {submitted && !content &&
                                <div className="help-block">Content is required</div>
                            }
                        </div>
                        <div className="form-group">
                            <button className="btn btn-primary"><span className="glyphicon glyphicon-send" aria-hidden="true"></span> Share</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

function mapState(state) {
    console.log(state);
    const { authentication } = state;
    const { user } = authentication;
    return { user };
}

const actionCreators = {
    submitPost: postActions.submitPost,
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export { connectedHomePage as HomePage };