import { postConstants } from '../_constants';
import { postService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const postActions = {
    getAll,
    submitPost,
};

function getAll() {
    return dispatch => {
        dispatch(request());

        postService.getAllPosts()
            .then(
                posts => dispatch(success(posts.results)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: postConstants.GETALL_REQUEST } }
    function success(posts) { return { type: postConstants.GETALL_SUCCESS, posts } }
    function failure(error) { return { type: postConstants.GETALL_FAILURE, error } }
}

function submitPost(content) {
    return dispatch => {
        dispatch(request({ content }));

        postService.submitPost(content)
            .then(
                post => { 
                    dispatch(success(post));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(post) { return { type: postConstants.SUMBMIT_REQUEST, post } }
    function success(post) { return { type: postConstants.SUMBMIT_SUCCESS, post } }
    function failure(error) { return { type: postConstants.SUMBMIT_FAILURE, error } }
}


// prefixed function name with underscore because delete is a reserved word in javascript
// function _delete(id) {
//     return dispatch => {
//         dispatch(request(id));

//         userService.delete(id)
//             .then(
//                 user => dispatch(success(id)),
//                 error => dispatch(failure(id, error.toString()))
//             );
//     };

//     function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
//     function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
//     function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
// }