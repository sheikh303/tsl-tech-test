import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';

import {postActions} from '../_actions';

class App extends React.Component {
    constructor(props) {
        super(props);

        history.listen((location, action) => {
            // clear alert on location change
            this.props.clearAlerts();
        });
    }

    componentDidMount() {
        this.props.getPosts();
    }


    render() {
        const { alert, posts } = this.props;
        return (
            <div className="container-fluid">
                <nav className="navbar navbar-default navbar-static-top">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#">TSL-WALL</a>
                    </div>
                </nav>
                <div className="col-sm-8 col-sm-offset-2">
                    {alert.message &&
                        <div className={`alert ${alert.type}`}>{alert.message}</div>
                    }
                    <Router history={history}>
                        <Switch>
                            <PrivateRoute exact path="/" component={HomePage} />
                            <Route path="/login" component={LoginPage} />
                            <Route path="/register" component={RegisterPage} />
                            <Redirect from="*" to="/" />
                        </Switch>
                    </Router>
                </div>
                <div className="col-sm-8 col-sm-offset-2">
                    <div className="text-center py-3">
                    <h3><span className="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> All Posts</h3>
                    </div>                
                    {posts.loading && <em>Loading posts...</em>}
                    {posts.error && <span className="text-danger">ERROR: {posts.error}</span>}
                    {posts.items &&
                        <div>
                            {posts.items.map((post, index) =>
                                    <div key={ post.id} id={post.id} className="panel panel-primary">                                        
                                        <div className="panel-body">
                                            { post.content }
                                        </div>
                                        <div className="panel-footer"><span className="glyphicon glyphicon-user" aria-hidden="true"></span> <b>{ post.creator}</b> On { post.created_at}</div>
                                    </div>
                            )}
                        </div>
                    }
                </div>
            </div>
        );
    }
}

function mapState(state) {
    const { alert, posts } = state;
    return { alert, posts };
}

const actionCreators = {
    clearAlerts: alertActions.clear,
    getPosts: postActions.getAll
};

const connectedApp = connect(mapState, actionCreators)(App);
export { connectedApp as App };