from django.db import models

# Create your models here.
from django.dispatch import receiver
from django.urls import reverse
from django_rest_passwordreset.signals import reset_password_token_created
from django.core.mail import send_mail  
from django.contrib.sites.models import Site


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    current_site = Site.objects.get_current()
    email_plaintext_message = current_site.domain+"{}?token={}".format(reverse('password_reset:reset-password-request'), reset_password_token.key)

    send_mail(
        # Subject:
        "[{title}]: Password Reset".format(title="Some website title"),
        # message:
        email_plaintext_message,
        # from:
        "sheikh303@yandex.com",
        # to:
        [reset_password_token.user.email]
    )