from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from rest_framework.test import APIClient
from .models import Post
import json

# Create your tests here.
User = get_user_model()

class PostTestCase(TestCase):  

    def setUp(self):
        self.c = Client()    

    def create_new_user(self):
        user = User.objects.create_user('john', 'sheikh303@yandex.com', 'Password@123')
        return user

    def create_new_posts(self):
        creator = User.objects.get(id=1)

        post = Post(content='Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.',
                    creator=creator)
        post.save()
        post = Post(content='Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.',
                    creator=creator)
        post.save()

    def test_add_new_post_to_wall(self):
        """
        Logged in users can add new post to wall
        """
        self.create_new_user()
        self.c.login(username='john', password='Password@123')
        
        login_response = self.c.post('/api/login/',
                               json.dumps(
                                   {'username': 'john',
                                    'password': 'Password@123' }),
                               content_type="application/json")
        token = "Token "+login_response.json()['token']   

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION= token)
        response = client.post('/api/v1/posts/', json.dumps(
                                   {'content': 'Hello World!!', }),
                               content_type="application/json")
                               
        assert 201 == response.status_code

        post = Post.objects.get(id=1)
        assert 'Hello World!!' == post.content

    def test_anonymous_cant_post(self):
        """
        Only logged user can add post
        """
        response = self.c.post('/api/v1/posts/',
                               json.dumps(
                                   {'content': 'Hello World!!', }),
                               content_type="application/json")
        assert 403 == response.status_code
        assert 'You must be authenticated' == response.json()['detail']

    def test_get_all_posts(self):
        """
        Get all posts, For Anonymous users
        """
        self.create_new_user()
        self.create_new_posts()
        response = self.c.get('/api/v1/posts/all',
                              content_type="application/json")
        
        assert 200 == response.status_code
        assert 2 == response.json()['count']        
        assert response.json()['results'][0]['content'].startswith('Contrary to popular belief')
        assert response.json()['results'][1]['content'].startswith('Lorem Ipsum is simply dummy text')
