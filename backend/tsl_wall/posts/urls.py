from django.urls import include, path, re_path
from .views import *

urlpatterns = [
    re_path(r'api/v1/posts/(?P<pk>[0-9]+)$', get_delete_update_post.as_view(), name='get_delete_update_post'),
    path('api/v1/posts/', post_post.as_view(), name='post_posts'),
    path('api/v1/posts/all', get_post.as_view(), name='get_posts'),
]