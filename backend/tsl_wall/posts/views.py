from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import RetrieveUpdateDestroyAPIView, CreateAPIView, ListAPIView
from .models import Post
from .permissions import IsOwnerOrReadOnly, IsAuthenticated
from .serializers import PostSerializer
from .pagination import CustomPagination

class get_delete_update_post(RetrieveUpdateDestroyAPIView):
    serializer_class = PostSerializer
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly,)

    def get_queryset(self, pk):
        try:
            post = Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            content = {
                'status': 'Not Found'
            }
            return Response(content, status=status.HTTP_404_NOT_FOUND)
        return post

    # Get a movie
    def get(self, request, pk):

        post = self.get_queryset(pk)
        serializer = PostSerializer(post)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # Update a movie
    def put(self, request, pk):
        
        post = self.get_queryset(pk)

        if(request.user == post.creator): # If creator is who makes request
            serializer = PostSerializer(post, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            content = {
                'status': 'UNAUTHORIZED'
            }
            return Response(content, status=status.HTTP_401_UNAUTHORIZED)

    # Delete a movie
    def delete(self, request, pk):

        post = self.get_queryset(pk)

        if(request.user == post.creator): # If creator is who makes request
            post.delete()
            content = {
                'status': 'NO CONTENT'
            }
            return Response(content, status=status.HTTP_204_NO_CONTENT)
        else:
            content = {
                'status': 'UNAUTHORIZED'
            }
            return Response(content, status=status.HTTP_401_UNAUTHORIZED)
   
class get_post(ListAPIView):
    serializer_class = PostSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination

    def get_queryset(self):
       post = Post.objects.all()
       return post

    # Get all posts
    def get(self, request):
        posts = self.get_queryset()
        paginate_queryset = self.paginate_queryset(posts)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)


class post_post(CreateAPIView):
    serializer_class = PostSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = CustomPagination  

    # Create a new post
    def post(self, request):
        serializer = PostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(creator=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

