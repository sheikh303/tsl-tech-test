from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    content = models.TextField(max_length=160)
    created_at = models.DateTimeField(auto_now_add=True) # When it was create
    updated_at = models.DateTimeField(auto_now=True) # When i was update
    creator = models.ForeignKey('auth.User', related_name='post', on_delete=models.CASCADE)

    class Meta:
        ordering = ['-id']
