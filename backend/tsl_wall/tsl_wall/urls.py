from django.contrib import admin
from django.urls import include, path
from rest_framework import routers

from django_email_verification import urls as mail_urls
from posts import urls as post_urls


router = routers.DefaultRouter()
# router.register(r'users', views.RegisterAPI)

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),

    #Registration & Login
    path('', include('accounts.urls')),
    path('api/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),
    path('email/', include(mail_urls)),
    path('', include(post_urls)),
]
